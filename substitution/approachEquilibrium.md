# Stabilizing Assumptions

* Shared immunity
* Sustained immunity
* No further changes to social mobility
* R=9.2 for BA.1 ;  R=12 for BA.2

## Transitional R.

The effective reproduction number $\tilde{R}$ is equal to $\tilde{R} =
S R_{0}$, where $R_{0}$ is the the reproduction number in a clear
environment, and where $S$ is the proportion of the population
suceptible.  Under our assumptions, if $R_{0}=12$, and
$\tilde{R}=1.2$, then $S=\frac{\tilde{R}}{R_{0}}$, so that $S=.1$.  In
order to neutralize the epidemic, we need $S=\frac{1}{12}$, so that
the infection rate would have to be 1666 out of 100K.  If this is a
spike, then we would be looking at 